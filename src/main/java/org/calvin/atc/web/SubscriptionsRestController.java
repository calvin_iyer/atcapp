package org.calvin.atc.web;

import org.calvin.atc.json.ResponseErrorCodes;
import org.calvin.atc.json.ResponseJson;
import org.calvin.atc.json.Subscription;
import org.calvin.atc.service.SubscriptionService;
import org.calvin.atc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author CalvinI
 *
 */

@RestController
@RequestMapping("/subscription")
public class SubscriptionsRestController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	SubscriptionService subscriptionService;
	
	@RequestMapping(value="/create",method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseJson createSubscriptionHandler(@RequestParam(value="url",required=false) String eventUrl) throws Exception{
		ResponseJson responseJson;
		Subscription subscription = subscriptionService.fetchSubscription(eventUrl);
		if(subscription == null) {
			responseJson = getFailureResponse("Error creating subscription!");
			responseJson.setSuccess(false);
		} else {
			responseJson = userService.createUser(subscription);
		}
		return responseJson;
	}
	
	@RequestMapping(value="/change", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseJson changeSubscription(@RequestParam(value="url",required=false) String eventUrl) throws Exception{
		ResponseJson responseJson;
		Subscription subscription = subscriptionService.fetchSubscription(eventUrl);
		if(subscription == null) {
			responseJson = getFailureResponse("Error changing subscription!");
		} else {
			responseJson = userService.changeUserSubscription(subscription);
		}
		return responseJson;
	}
	
	@RequestMapping(value="/cancel", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseJson cancelSubscription(@RequestParam(value="url",required=false) String eventUrl) throws Exception{
		ResponseJson responseJson;
		Subscription subscription = subscriptionService.fetchSubscription(eventUrl);
		if(subscription == null) {
			responseJson = getFailureResponse("Error changing subscription!");
		} else {
			responseJson = userService.cancelUserSubscription(subscription);
		}
		return responseJson;
	}
	
	@RequestMapping(value="/status", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseJson subscriptionStatus(@RequestParam(value="url",required=false) String eventUrl) throws Exception{
		ResponseJson responseJson;
		Subscription subscription = subscriptionService.fetchSubscription(eventUrl);
		if(subscription == null) {
			responseJson = getFailureResponse("Error getting status of subscription!");
		} else {
			responseJson = userService.subscriptionStatusNotification(subscription);
		}
		return responseJson;
	}
	
	private ResponseJson getFailureResponse(String msg) {
		ResponseJson responseJson = new ResponseJson();
		responseJson.setErrorCode(ResponseErrorCodes.UNKNOWN_ERROR);
		responseJson.setMessage(msg);
		responseJson.setSuccess(false);
		return responseJson;
	}

}
