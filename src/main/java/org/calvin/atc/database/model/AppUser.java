package org.calvin.atc.database.model;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.calvin.atc.json.AccountStatusType;
import org.calvin.atc.json.OrderInfo;
import org.calvin.atc.json.UserInfo;

/**
 * @author CalvinI
 *
 */

@Entity
public class AppUser extends UserInfo {
	
	@Id
	private String id;
	
	@Embedded
	private OrderInfo orderInfo;
	
	private AccountStatusType accountStatus;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public OrderInfo getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(OrderInfo orderInfo) {
		this.orderInfo = orderInfo;
	}
	
	public AccountStatusType getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(AccountStatusType accountStatus) {
		this.accountStatus = accountStatus;
	}

	public void setUserInfo(UserInfo userInfo) {
		setEmail(userInfo.getEmail());
		setFirstName(userInfo.getFirstName());
		setLanguage(userInfo.getLanguage());
		setLastName(userInfo.getLastName());
		setLocale(userInfo.getLocale());
		setOpenId(userInfo.getOpenId());
		setUuid(userInfo.getUuid());
		setAddress(userInfo.getAddress());
	}

	@Override
	public String toString() {
		return "AppUser [id=" + id + ", orderInfo=" + orderInfo + ", email=" + email + ", firstName=" + firstName
				+ ", language=" + language + ", lastName=" + lastName + ", locale=" + locale + ", openId=" + openId
				+ ", uuid=" + uuid + ", address=" + address + "]";
	}

}
