package org.calvin.atc.database.repo;

import org.calvin.atc.database.model.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author CalvinI
 *
 */

@Repository
public interface AppUserRepo extends JpaRepository<AppUser, String> {

}
