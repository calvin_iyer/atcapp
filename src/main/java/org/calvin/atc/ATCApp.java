package org.calvin.atc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author CalvinI
 *
 */

@SpringBootApplication
public class ATCApp 
{
    public static void main( String[] args )
    {
        SpringApplication.run(ATCApp.class, args);
    }
}
