package org.calvin.atc.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.calvin.atc.json.Subscription;
import org.calvin.atc.service.SubscriptionService;
import org.calvin.atc.util.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;

/**
 * @author CalvinI
 *
 */

@Service
public class SubscriptionServiceImpl implements SubscriptionService {

	private static final Logger logger = LoggerFactory.getLogger(SubscriptionServiceImpl.class);
	
	@Override
	public Subscription fetchSubscription(String eventUrl) {
		try {
			OAuthConsumer consumer = new CommonsHttpOAuthConsumer(Constants.OAUTH_CONSUMER_KEY,Constants.OAUTH_CONSUMER_SECRET);
			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpGet httpGet = new HttpGet(eventUrl);
			httpGet.setHeader("Accept", "application/json");
			consumer.sign(httpGet);
			HttpResponse eventResponse = httpClient.execute(httpGet);
			logger.debug("Response Code : " + eventResponse.getStatusLine().getStatusCode());
	
			BufferedReader rd = new BufferedReader(new InputStreamReader(eventResponse.getEntity().getContent()));
	
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			logger.debug("Event details : "+result);
			Gson gson = new Gson();
			Subscription subscription = gson.fromJson(result.toString(), Subscription.class);
			logger.debug("eventurl data:"+subscription.toString());
			return subscription;
		} catch(Exception e) {
			logger.error("Error fetching event details",e);
		}
		return null;
	}

}
