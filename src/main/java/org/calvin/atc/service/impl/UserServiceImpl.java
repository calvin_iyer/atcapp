package org.calvin.atc.service.impl;

import java.util.UUID;

import org.calvin.atc.database.model.AppUser;
import org.calvin.atc.database.repo.AppUserRepo;
import org.calvin.atc.json.AccountStatusType;
import org.calvin.atc.json.Notice;
import org.calvin.atc.json.NoticeType;
import org.calvin.atc.json.ResponseErrorCodes;
import org.calvin.atc.json.ResponseJson;
import org.calvin.atc.json.Subscription;
import org.calvin.atc.json.UserInfo;
import org.calvin.atc.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author CalvinI
 *
 */

@Service
public class UserServiceImpl implements UserService {

	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
	AppUserRepo appUserRepo;
	
	@Override
	public ResponseJson createUser(Subscription subscription) {
		ResponseJson response = new ResponseJson();
		UserInfo userInfo = null;
		try {
			if(subscription.getCreator() == null && subscription.getPayload().getUser() == null) {
				response.setErrorCode(ResponseErrorCodes.UNKNOWN_ERROR);
				response.setMessage("Error creating subscription!");
				response.setSuccess(false);
			} else {
				userInfo = subscription.getCreator() == null ? subscription.getPayload().getUser():subscription.getCreator();
				AppUser appUser = new AppUser();
				appUser.setId(UUID.randomUUID().toString());
				appUser.setUserInfo(userInfo);
				appUser.setOrderInfo(subscription.getPayload().getOrder());
				appUser.setAccountStatus(AccountStatusType.ACTIVE);
				appUserRepo.save(appUser);
				response.setAccountIdentifier(appUser.getId());
				response.setMessage("Successfully created subscription!");
				response.setSuccess(true);
			}
		} catch(Exception e) {
			logger.error("Error creating user", e);
			response.setErrorCode(ResponseErrorCodes.UNKNOWN_ERROR);
			response.setMessage("Error creating subscription!");
			response.setSuccess(false);
		}
		return response;
	}

	@Override
	public ResponseJson changeUserSubscription(Subscription subscription) {
		ResponseJson response = new ResponseJson();
		try {
			if(subscription.getPayload().getAccount() == null) {
				response.setErrorCode(ResponseErrorCodes.UNKNOWN_ERROR);
				response.setMessage("Error changing subscription!");
				response.setSuccess(false);
			} else {
				String userId = subscription.getPayload().getAccount().getAccountIdentifier();
				AppUser appUser = appUserRepo.findOne(userId);
				if(appUser == null) {
					response.setErrorCode(ResponseErrorCodes.USER_NOT_FOUND);
					response.setMessage("Invalid User!");
					response.setSuccess(false);
				} else {
					appUser.setOrderInfo(subscription.getPayload().getOrder());
					appUserRepo.save(appUser);
					response.setAccountIdentifier(appUser.getId());
					response.setMessage("Successfully changed subscription!");
					response.setSuccess(true);
				}
			}
		} catch(Exception e) {
			logger.error("Error changin user subscription", e);
			response.setErrorCode(ResponseErrorCodes.UNKNOWN_ERROR);
			response.setMessage("Error changing subscription!");
			response.setSuccess(false);
		}
		return response;
	}

	@Override
	public ResponseJson cancelUserSubscription(Subscription subscription) {
		ResponseJson response = new ResponseJson();
		try {
			if(subscription.getPayload().getAccount() == null) {
				response.setErrorCode(ResponseErrorCodes.UNKNOWN_ERROR);
				response.setMessage("Error cancelling subscription!");
				response.setSuccess(false);
			} else {
				String userId = subscription.getPayload().getAccount().getAccountIdentifier();
				AppUser appUser = appUserRepo.findOne(userId);
				if(appUser == null) {
					response.setErrorCode(ResponseErrorCodes.USER_NOT_FOUND);
					response.setMessage("Invalid User!");
					response.setSuccess(false);
				} else {
					appUser.setAccountStatus(AccountStatusType.CANCELLED);
					appUserRepo.save(appUser);
					response.setAccountIdentifier(appUser.getId());
					response.setMessage("Successfully cancelled subscription!");
					response.setSuccess(true);
				}
			}
		} catch(Exception e) {
			logger.error("Error cancelling user subscription", e);
			response.setErrorCode(ResponseErrorCodes.UNKNOWN_ERROR);
			response.setMessage("Error cancelling subscription!");
			response.setSuccess(false);
		}
		return response;
	}

	@Override
	public ResponseJson subscriptionStatusNotification(Subscription subscription) {
		ResponseJson response = new ResponseJson();
		try {
			if(subscription.getPayload().getAccount() == null || subscription.getPayload().getNotice() == null) {
				response.setErrorCode(ResponseErrorCodes.UNKNOWN_ERROR);
				response.setMessage("Error while processing subscription status!");
				response.setSuccess(false);
			} else {
				String userId = subscription.getPayload().getAccount().getAccountIdentifier();
				AppUser appUser = appUserRepo.findOne(userId);
				if(appUser == null) {
					response.setErrorCode(ResponseErrorCodes.USER_NOT_FOUND);
					response.setMessage("Invalid User!");
					response.setSuccess(false);
				} else {
					Notice subscriptionNotice = subscription.getPayload().getNotice(); 
					if(NoticeType.CLOSED.equals(subscriptionNotice.getType()))
						appUser.setAccountStatus(AccountStatusType.CANCELLED);
					else if(NoticeType.DEACTIVATED.equals(subscriptionNotice.getType()))
						appUser.setAccountStatus(AccountStatusType.SUSPENDED);
					else if(NoticeType.REACTIVATED.equals(subscriptionNotice.getType()))
						appUser.setAccountStatus(AccountStatusType.ACTIVE);
					appUserRepo.save(appUser);
					response.setAccountIdentifier(appUser.getId());
					response.setMessage("Successfully processed subscription status notification!");
					response.setSuccess(true);
				}
			}
		} catch(Exception e) {
			logger.error("Error while processing subscription status!", e);
			response.setErrorCode(ResponseErrorCodes.UNKNOWN_ERROR);
			response.setMessage("Error while processing subscription status!");
			response.setSuccess(false);
		}
		return response;
	}

}
