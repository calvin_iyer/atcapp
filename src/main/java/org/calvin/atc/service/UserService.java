package org.calvin.atc.service;

import org.calvin.atc.json.ResponseJson;
import org.calvin.atc.json.Subscription;

/**
 * @author CalvinI
 *
 */

public interface UserService {
	
	public ResponseJson createUser(Subscription subscription);
	
	public ResponseJson changeUserSubscription(Subscription subscription);
	
	public ResponseJson cancelUserSubscription(Subscription subscription);
	
	public ResponseJson subscriptionStatusNotification(Subscription subscription);

}
