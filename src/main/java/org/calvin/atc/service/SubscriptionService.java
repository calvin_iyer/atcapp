package org.calvin.atc.service;

import org.calvin.atc.json.Subscription;

/**
 * @author CalvinI
 *
 */

public interface SubscriptionService {
	
	public Subscription fetchSubscription(String eventUrl);

}
