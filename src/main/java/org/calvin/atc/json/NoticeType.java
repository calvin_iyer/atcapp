package org.calvin.atc.json;

/**
 * @author CalvinI
 *
 */

public enum NoticeType {
	REACTIVATED, DEACTIVATED, CLOSED
}
