package org.calvin.atc.json;

import javax.persistence.Embeddable;

/**
 * @author CalvinI
 *
 */

@Embeddable
public class OrderInfo {
	
	private String editionCode;
	
	private String addonOfferingCode;
	
	private String pricingDuration;

	public String getEditionCode() {
		return editionCode;
	}

	public void setEditionCode(String editionCode) {
		this.editionCode = editionCode;
	}

	public String getAddonOfferingCode() {
		return addonOfferingCode;
	}

	public void setAddonOfferingCode(String addonOfferingCode) {
		this.addonOfferingCode = addonOfferingCode;
	}

	public String getPricingDuration() {
		return pricingDuration;
	}

	public void setPricingDuration(String pricingDuration) {
		this.pricingDuration = pricingDuration;
	}

	@Override
	public String toString() {
		return "OrderInfo [editionCode=" + editionCode + ", addonOfferingCode=" + addonOfferingCode
				+ ", pricingDuration=" + pricingDuration + "]";
	}
	
	
}
