package org.calvin.atc.json;

/**
 * @author CalvinI
 *
 */

public class AccountInfo {

	private String accountIdentifier;
	
	private AccountStatusType status;

	public String getAccountIdentifier() {
		return accountIdentifier;
	}

	public void setAccountIdentifier(String accountIdentifier) {
		this.accountIdentifier = accountIdentifier;
	}

	public AccountStatusType getStatus() {
		return status;
	}

	public void setStatus(AccountStatusType status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "AccountInfo [accountIdentifier=" + accountIdentifier + ", status=" + status + "]";
	}

	
}
