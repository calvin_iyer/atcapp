package org.calvin.atc.json;

/**
 * @author CalvinI
 *
 */

public class Subscription {
	
	private SubscriptionEventType type;
	
	private MarketPlace marketplace;
	
	private String applicationUuid;
	
	private String flag;
	
	private String returnUrl;
	
	private UserInfo creator;
	
	private Payload payload;

	public SubscriptionEventType getType() {
		return type;
	}

	public void setType(SubscriptionEventType type) {
		this.type = type;
	}

	public MarketPlace getMarketplace() {
		return marketplace;
	}

	public void setMarketplace(MarketPlace marketplace) {
		this.marketplace = marketplace;
	}

	public String getApplicationUuid() {
		return applicationUuid;
	}

	public void setApplicationUuid(String applicationUuid) {
		this.applicationUuid = applicationUuid;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	public UserInfo getCreator() {
		return creator;
	}

	public void setCreator(UserInfo creator) {
		this.creator = creator;
	}

	public Payload getPayload() {
		return payload;
	}

	public void setPayload(Payload payload) {
		this.payload = payload;
	}

	@Override
	public String toString() {
		return "Subscription [type=" + type + ", marketplace=" + marketplace + ", applicationUuid=" + applicationUuid
				+ ", flag=" + flag + ", returnUrl=" + returnUrl + ", creator=" + creator + ", payload=" + payload + "]";
	}
	
}
