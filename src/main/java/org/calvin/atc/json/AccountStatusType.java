package org.calvin.atc.json;

/**
 * @author CalvinI
 *
 */

public enum AccountStatusType {
	
	INITIALIZED, FAILED, FREE_TRIAL, FREE_TRIAL_EXPIRED, ACTIVE, SUSPENDED, CANCELLED

}
