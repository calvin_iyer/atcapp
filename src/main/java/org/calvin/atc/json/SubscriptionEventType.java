package org.calvin.atc.json;

/**
 * @author CalvinI
 *
 */

public enum SubscriptionEventType {
	
	SUBSCRIPTION_ORDER, SUBSCRIPTION_CHANGE, SUBSCRIPTION_CANCEL, 
	SUBSCRIPTION_NOTICE, USER_ASSIGNMENT, USER_UNASSIGNMENT, USER_UPDATED

}
