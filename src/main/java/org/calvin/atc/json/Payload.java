package org.calvin.atc.json;

/**
 * @author CalvinI
 *
 */

public class Payload {

	private UserInfo user;
	
	private AccountInfo account;
	
	private CompanyInfo company;
	
	private OrderInfo order;
	
	private Notice notice;

	public UserInfo getUser() {
		return user;
	}

	public void setUser(UserInfo user) {
		this.user = user;
	}

	public AccountInfo getAccount() {
		return account;
	}

	public void setAccount(AccountInfo account) {
		this.account = account;
	}

	public CompanyInfo getCompany() {
		return company;
	}

	public void setCompany(CompanyInfo company) {
		this.company = company;
	}

	public OrderInfo getOrder() {
		return order;
	}

	public void setOrder(OrderInfo order) {
		this.order = order;
	}

	public Notice getNotice() {
		return notice;
	}

	public void setNotice(Notice notice) {
		this.notice = notice;
	}

	@Override
	public String toString() {
		return "Payload [user=" + user + ", account=" + account + ", company=" + company + ", order=" + order
				+ ", notice=" + notice + "]";
	}
	
	
}
